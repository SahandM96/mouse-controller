# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'layout.ui'
#
# Created by: PyQt5 UI code generator 5.14.2
#
# WARNING! All changes made in this file will be lost!

from pynput.mouse import Button, Controller
from datetime import datetime, date, time, timedelta
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtGui import QMouseEvent
import threading
from datetime import datetime, date, time, timedelta
from time import sleep
import schedule

mouse = Controller()

QtWidgets.QApplication.setAttribute(QtCore.Qt.AA_EnableHighDpiScaling, True)  # enable highdpi scaling
QtWidgets.QApplication.setAttribute(QtCore.Qt.AA_UseHighDpiPixmaps, True)  # use highdpi icons


class Ui_MainWindow(object):

    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(695, 250)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("GUI/favicon.ico"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        MainWindow.setWindowIcon(icon)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setMinimumSize(QtCore.QSize(492, 143))
        self.centralwidget.setObjectName("centralwidget")
        self.groupBox_4 = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_4.setGeometry(QtCore.QRect(340, 0, 221, 250))
        self.groupBox_4.setObjectName("groupBox_4")
        self.timeFormatLabel = QtWidgets.QLabel(self.groupBox_4)
        self.timeFormatLabel.setGeometry(QtCore.QRect(40, 60, 141, 20))
        self.timeFormatLabel.setObjectName("timeFormatLabel")
        self.inputTimeH = QtWidgets.QTextEdit(self.groupBox_4)
        self.inputTimeH.setGeometry(QtCore.QRect(10, 110, 44, 30))
        self.inputTimeH.setObjectName("inputTimeH")
        self.inputTimeM = QtWidgets.QTextEdit(self.groupBox_4)
        self.inputTimeM.setGeometry(QtCore.QRect(60, 110, 44, 30))
        self.inputTimeM.setObjectName("inputTimeM")
        self.inputTimeS = QtWidgets.QTextEdit(self.groupBox_4)
        self.inputTimeS.setGeometry(QtCore.QRect(110, 110, 44, 30))
        self.inputTimeS.setObjectName("inputTimeS")
        self.inputTimeMi = QtWidgets.QTextEdit(self.groupBox_4)
        self.inputTimeMi.setGeometry(QtCore.QRect(160, 110, 44, 30))
        self.inputTimeMi.setObjectName("inputTimeMi")
        self.setTimeButton = QtWidgets.QPushButton(self.groupBox_4)
        self.setTimeButton.setGeometry(QtCore.QRect(20, 210, 171, 20))
        self.setTimeButton.setObjectName("setTimeButton")
        self.groupBox_3 = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_3.setGeometry(QtCore.QRect(160, 0, 171, 250))
        self.groupBox_3.setObjectName("groupBox_3")
        self.inputX = QtWidgets.QTextEdit(self.groupBox_3)
        self.inputX.setGeometry(QtCore.QRect(30, 70, 121, 30))
        self.inputX.setObjectName("inputX")
        self.inputY = QtWidgets.QTextEdit(self.groupBox_3)
        self.inputY.setGeometry(QtCore.QRect(30, 120, 121, 30))
        self.inputY.setObjectName("inputY")
        self.label_5 = QtWidgets.QLabel(self.groupBox_3)
        self.label_5.setGeometry(QtCore.QRect(10, 70, 20, 20))
        self.label_5.setObjectName("label_5")
        self.label_6 = QtWidgets.QLabel(self.groupBox_3)
        self.label_6.setGeometry(QtCore.QRect(10, 120, 20, 20))
        self.label_6.setObjectName("label_6")
        self.setXandYButton = QtWidgets.QPushButton(self.groupBox_3)
        self.setXandYButton.setGeometry(QtCore.QRect(10, 210, 151, 20))
        self.setXandYButton.setObjectName("setXandYButton")
        self.groupBox_2 = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_2.setGeometry(QtCore.QRect(0, 0, 151, 250))
        self.groupBox_2.setObjectName("groupBox_2")
        self.currentXshow = QtWidgets.QLabel(self.groupBox_2)
        self.currentXshow.setGeometry(QtCore.QRect(60, 70, 61, 20))
        self.currentXshow.setObjectName("currentXshow")
        self.label = QtWidgets.QLabel(self.groupBox_2)
        self.label.setGeometry(QtCore.QRect(8, 70, 41, 20))
        self.label.setObjectName("label")
        self.currentYshow = QtWidgets.QLabel(self.groupBox_2)
        self.currentYshow.setGeometry(QtCore.QRect(60, 120, 61, 20))
        self.currentYshow.setObjectName("currentYshow")
        self.label_3 = QtWidgets.QLabel(self.groupBox_2)
        self.label_3.setGeometry(QtCore.QRect(8, 120, 41, 20))
        self.label_3.setObjectName("label_3")
        self.groupBox_5 = QtWidgets.QGroupBox(self.centralwidget)
        self.groupBox_5.setGeometry(QtCore.QRect(570, 0, 121, 250))
        self.groupBox_5.setObjectName("groupBox_5")
        # self.repeatEveryDay = QtWidgets.QCheckBox(self.groupBox_5)
        # self.repeatEveryDay.setEnabled(True)
        # self.repeatEveryDay.setGeometry(QtCore.QRect(10, 60, 69, 20))
        # self.repeatEveryDay.setObjectName("repeatEveryDay")
        self.StartButton = QtWidgets.QPushButton(self.groupBox_5)
        self.StartButton.setGeometry(QtCore.QRect(10, 210, 80, 20))
        self.StartButton.setObjectName("StartButton")
        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "Mouse-Controller-By:SahandM96"))
        self.groupBox_4.setTitle(_translate("MainWindow", "Set Time to Click"))
        self.timeFormatLabel.setText(_translate("MainWindow", "Time Format : HH:MM:SS:m"))
        self.inputTimeH.setPlaceholderText(_translate("MainWindow", "HH"))
        self.inputTimeM.setPlaceholderText(_translate("MainWindow", "MM"))
        self.inputTimeS.setPlaceholderText(_translate("MainWindow", "SS"))
        self.inputTimeMi.setPlaceholderText(_translate("MainWindow", "m"))
        self.setTimeButton.setText(_translate("MainWindow", "Set Time"))
        self.groupBox_3.setTitle(_translate("MainWindow", "Set X and Y In Here"))
        self.label_5.setText(_translate("MainWindow", "X"))
        self.label_6.setText(_translate("MainWindow", "Y"))
        self.setXandYButton.setText(_translate("MainWindow", "Set X and Y"))
        self.groupBox_2.setTitle(_translate("MainWindow", "Current X and Y"))
        self.currentXshow.setText(_translate("MainWindow", "X"))
        self.label.setText(_translate("MainWindow", "Current X"))
        self.currentYshow.setText(_translate("MainWindow", "Y"))
        self.label_3.setText(_translate("MainWindow", "Current Y"))
        self.groupBox_5.setTitle(_translate("MainWindow", "Repeat"))
        # self.repeatEveryDay.setText(_translate("MainWindow", "RepeatEveryDay"))
        self.setXandYButton.clicked.connect(set_x_and_y_mouse)
        self.StartButton.setText(_translate("MainWindow", "Repeat"))
        self.setTimeButton.clicked.connect(check_time)
        self.StartButton.clicked.connect(repeat_loop)


def set_x_and_y_mouse():
    x_pos, y_pos = get_input_x_and_input_y()
    # x_pos = int(x_pos)
    # y_pos = int(y_pos)
    ui.currentYshow.setText(y_pos)
    ui.currentXshow.setText(x_pos)
    mouse.position = (int(x_pos), int(y_pos))


def get_input_x_and_input_y():
    x = ui.inputX.toPlainText().strip()
    y = ui.inputY.toPlainText().strip()
    return x, y


def get_time():
    HH = ui.inputTimeH.toPlainText().strip()
    MM = ui.inputTimeM.toPlainText().strip()
    SS = ui.inputTimeS.toPlainText().strip()
    mil = ui.inputTimeMi.toPlainText().strip()
    return HH, MM, SS


def check_time():
    HH, MM, SS = get_time()
    timer = time(int(HH), int(MM), int(SS))
    # schedule.every().day.at(timer).do(print("Hora"))
    while True:
        if datetime.now().time().hour == timer.hour and \
                datetime.now().time().minute == timer.minute and \
                datetime.now().time().second == timer.second:
            click_and_repeat()
            break
        else:
            print(f'my {timer} sys {datetime.now().time()}')


def click_and_repeat():
    set_x_and_y_mouse()
    mouse.click(Button.left, 3)


def repeat_loop():
    check_time()


if __name__ == "__main__":
    import sys

    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    sys.exit(app.exec_())
